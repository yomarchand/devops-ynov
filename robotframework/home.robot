*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Test Cases ***
Valid Home Page
    Title Should Be     Rage Freebie HTML5 Landing page
    Wait Until Element Is Not Visible   //div[@id="loading"]
    Click Element    //a[contains(text(),'GET STARTED')]
    Wait Until Page Contains    Hello
