<?php


class City
{
    public function getCityNameById($id)
    {
        if ($id == 1) {
            $city = "Bordeaux";
        } else {
            $city = "Paris";
        }
        return $city;
    }

    public function getCountryByCity($city)
    {
        $frenchCities = ['Paris', 'Marseille', 'Lyon', 'Bordeaux', 'Lille'];

        if (in_array($city, $frenchCities))
        {
            return 'France';
        } else {
            return 'Espagne';
        }

    }
}
