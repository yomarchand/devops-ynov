FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

EXPOSE 80

WORKDIR src /var/www/

CMD apachectl -D FOREGROUND


