<?php

require_once __DIR__ . '/../src/vendor/autoload.php';
// Autoload files using Composer autoload
include_once(__DIR__ . "/../src/City.php");

class CityTest extends \PHPUnit\Framework\TestCase
{
    public function testGetCityNameById()
    {
        $city = new City();
        $result = $city->getCityNameById(1);
        $expected = 'Bordeaux';
        $this->assertTrue($result == $expected);
    }

    public function testGetCountryByCity()
    {
        $city = new City();
        $country = $city->getCountryByCity('Lille');
        $expected = 'France';
        $this->assertTrue($country == $expected);

        $country = $city->getCountryByCity('Paris');
        $this->assertTrue($country == $expected);

        $country = $city->getCountryByCity('Madrid');
        $this->assertTrue($country == 'Espagne');
    }
}
